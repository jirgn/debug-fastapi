import uvicorn
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def root():
    a = "hello"
    b = "world"
    return {"message": a + " " + b}


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8004, reload=True)
