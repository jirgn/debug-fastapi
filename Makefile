.PHONY: dev-setup
dev-setup: dev-setup-venv dev-activate-venv

.PHONY: dev-setup-venv
dev-setup-venv: ## create virtual environment
	python -m venv .venv

.PHONY: dev-activate-venv
dev-activate-venv: ## activate virtual environment
	. venv/bin/activate

.PHONY: install
install: ## install dependencies
	pip install -r requirements.txt

.PHONY: dev-docker-build
dev-docker-build: ## build docker image
	docker build -t debug-fastapi .

.PHONY: dev-docker-run
dev-docker-run: ## run docker image
	docker run --name debug-fastapi --mount type=bind,source=$(PWD),target=/app -p 8004:8004 -p 5678:5678 debug-fastapi

.PHONY: dev-docker-start
dev-docker-start: ## start docker image
	docker start debug-fastapi

.PHONY: dev-docker-stop
dev-docker-stop: ## stop docker image
	docker stop debug-fastapi

.PHONY: help
help: ## print help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'	
