FROM python:3.11-slim

RUN apt update && apt install -y git

WORKDIR /code
COPY main.py requirements.txt ./
RUN pip install -r requirements.txt

CMD ["python", "-m", "debugpy", "--listen", "0.0.0.0:5678", "--wait-for-client", "main.py"]
