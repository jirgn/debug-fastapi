# Debug FastApi Application

This is a very simplistic FastApi app only
to show how to debug a FastApi application.

## Running the app

Use `make help` to get a list of available commands.
This Setup uses Docker for local
development and debugging environment. 

## Debugging

We use `debugpy` for debugging.

To enable remote debugging we need 2 things:
1. an entrypoint in the app to run the
   server when called via script 
2. a docker container that runs debugpy
   starting this the app as script (and
trigger above)
